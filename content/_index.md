+++
title = "Xingzhi Xie"
description = "Hello, everyone! Welcome to my personal website!"
+++

I am a dedicated and skilled professional currently pursuing a Master’s in Statistical Science at Duke University, with a background in Mathematics & Economics from UCLA. My academic journey is distinguished by a strong focus on courses like Predictive Modeling, Statistical Learning, and Bayesian Statistics. My professional experiences include impactful roles as a Finance Intern at the Bank of China and an Accounting Intern at Suzhou Jinding Accounting Firm. I have a proven track record in data analysis, financial evaluation, and research, evidenced by my involvement in projects like the Culinary Market Analysis using Yelp data and leading a team in analyzing real estate data in Branford, Connecticut. My technical proficiency spans R, SQL, Python, C++, and MATLAB, complemented by fluency in English and Mandarin. I am enthusiastic about applying my expertise in data science and statistical analysis to solve real-world challenges.






