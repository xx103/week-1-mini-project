
+++
title = "Culinary Market Analysis Tool Leveraging Yelp Data"
date = 2023-11-01  # Start date of the project
[taxonomies]
categories = ["Data Science", "Course Project"]
tags = ["R", "Shiny", "Data Visualization", "Yelp"]
+++

Developed an R-based Shiny dashboard to analyze consumer preferences and market dynamics across 9,990 restaurants in six international cuisine categories, utilizing over 1.16 million Yelp reviews.

- Incorporated data-driven investment strategies in the U.S. international culinary sector by performing comprehensive trend analysis and data preprocessing from 2010 onwards.
- Integrated SWOT analysis into the dashboard, providing strategic insights into the strengths, weaknesses, opportunities, and threats within the international food market.

---